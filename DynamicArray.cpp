/***************************************
 *Created by Dillon Connolly
 *DynamicArray program
 *September 1st, 2014
 *************************************/
#include <assert.h>
#include <iostream>
using namespace std;

template<class anyType> class node
{
public:
  node();
  anyType value;
  node *next;
};

template <class anyDataType> class dynamicArray
{
public:
  dynamicArray();
  void insertAtIndex(int index, anyDataType value);
  void insertAtHead(anyDataType value);
  void insertAtTail(anyDataType value);
  void swapItemsIndex(int index1, int index2);
  anyDataType retrieveFromIndex(int index);
  void removeFromIndex(int index);
  int getLengthArray();
  int getAmtPositionsAllocatedToMemory();
  void preAllocateMemory(int amountToAllocate);
  void getContentsOfArray();
private:
  node<anyDataType> *head;
  node<int> *allocatedMemory;
  int memberCount;
  int amtAllocated;
};

template <class anyType> node<anyType>::node()
{
  value = 0;
  next = NULL;
}

template <class anyDataType> dynamicArray<anyDataType>::dynamicArray()
{
  head = NULL;
  memberCount = 0;
  allocatedMemory = NULL;
  amtAllocated = 0;
}

template <class anyDataType> void dynamicArray<anyDataType>::insertAtIndex(int index, anyDataType value)
{
  node<anyDataType> *temp = new node<anyDataType>();
  temp->value = value;

  if(index > memberCount || index < 0)
    {
      cout<<"The index is invalid. It is greater than the amount of items in the array." << endl; 
    }
  else
    {
      if(index == 0)
	{
	  temp->next = head;
	  head = temp;
	  memberCount++;
	}
      else
	{
	  node<anyDataType> * traverse = new node<anyDataType>();
	  traverse = head;

	  for(int i = 0; i<index-1; i++)
	    {
	      traverse = traverse->next;
	    }
	  assert(traverse != NULL);
	  temp->next = traverse->next;
	  traverse->next = temp;
	  memberCount++;
	}
    }
}
template <class anyDataType> void dynamicArray<anyDataType>::insertAtHead(anyDataType value)
{
  node<anyDataType> *temp = new node<anyDataType>();
  temp->value = value;

  if (head == NULL) //No head yet in array. Make temp the head.
    {
      head = temp;
    }
  else //Assign temp as the new head then reassign head to be temp.
    {
      temp->next = head;
      head = temp;
    }
  memberCount++;
}
template <class anyDataType> void dynamicArray<anyDataType>::insertAtTail(anyDataType value)
{
  node<anyDataType> *temp = new node<anyDataType>();
  temp-> value = value;
  
  if(head == NULL) //Array has not been created yet. Tail is equal to head.
    {
      head = temp;
    }
  else //Array is one or more units long. Traverse to the end of the array and then add the new node to the end.
    {
      node<anyDataType> *traverse = new node<anyDataType>();
      traverse = head;
      while(traverse->next != NULL)
	{
	  traverse = traverse->next;
	}
      assert(traverse !=NULL);
      traverse->next = temp;
    }
  memberCount++;
}
template <class anyDataType> void dynamicArray<anyDataType>::swapItemsIndex(int index1, int index2)
{
  if(index1 > memberCount-1 || index1 < 0)
    {
      cout<<"Index 1 is out of bounds."<<endl;
    }
  if(index2 > memberCount-1 || index2 < 0)
    {
      cout<<"Index 2 is out of bounds."<<endl;
    }
  else
    {
      node<anyDataType> * traverse1 = new node<anyDataType>();
      node<anyDataType> * traverse2 = new node<anyDataType>();
      node<anyDataType> * swapVariable = new node<anyDataType>();
      traverse1 = head;
      traverse2 = head;

      /*start with both traverse nodes pointing at the head 
       *of the array. Then transition them until they are 
       *pointing at the correct index. Then swap the contents 
       *at traverse1 and traverse2. Delete swapVariable so it 
       *doesn't take up memory.
       */
      for(int i = 0; i<index1; i++)
	{
	  traverse1 = traverse1->next;
	}
      for(int i = 0; i<index2; i++)
	{
	  traverse2 = traverse2->next;
	}
      assert(traverse1 != NULL);
      assert(traverse2 != NULL);

      swapVariable-> value = traverse1->value;
      traverse1->value = traverse2->value;
      traverse2->value = swapVariable->value;
      delete swapVariable; 
    }
}
template <class anyDataType> anyDataType dynamicArray<anyDataType>::retrieveFromIndex(int index)
{
  if(head==NULL)
    {
      cout<<"There is no array to retrieve from."<<endl;
    }
  else
    {
      if(index>memberCount-1 || index < 0)
	{
	  cout<<"That index is out of bounds."<<endl;
	}
      else
	{
	  node<anyDataType> *traverse = new node<anyDataType>();
	  traverse = head;
	  for(int i = 0; i<index; i++)
	    {
	      traverse = traverse->next;
	    }
	  return traverse->value;
	}
    }
}
template <class anyDataType> void dynamicArray<anyDataType>::removeFromIndex(int index)
{
  if(head==NULL)
    {
      cout<<"There is no array to delete."<<endl;
    }
  if(index<0 || index> memberCount-1)
    {
      cout<<"Index is out of bounds."<<endl;
    }
  else
    {
      node<anyDataType> *traverse = new node<anyDataType>();
      node<anyDataType> *nodeToDelete = new node<anyDataType>();
      traverse = head;
      for(int i = 0; i<index-1; i++)
	{
	  traverse = traverse->next;
	}
      assert(traverse != NULL);
      assert(traverse->next != NULL);
      nodeToDelete = traverse->next;
      traverse->next = nodeToDelete->next;
      delete nodeToDelete;
      memberCount--;
    }
}
template <class anyDataType> int dynamicArray<anyDataType>::getLengthArray()
{
  return memberCount;
}
template <class anyDataType> int dynamicArray<anyDataType>::getAmtPositionsAllocatedToMemory()
{
  if(amtAllocated == 0)
    {
      return memberCount;
    }
  else
    {
      return (memberCount >= amtAllocated ? memberCount:amtAllocated);
    }
}
template <class anyDataType> void dynamicArray<anyDataType>::preAllocateMemory(int amountToAllocate)
{
  allocatedMemory  = new node<anyDataType>[amountToAllocate];
  amtAllocated = amountToAllocate;
}

template <class anyDataType> void dynamicArray<anyDataType>::getContentsOfArray()
{
  //This function was added for debugging purposes. It displays the contents of the array on the console. 
  node<anyDataType> *temp = new node<anyDataType>();
  temp = head;
  for(int i = 0; i < memberCount; i++){
    cout<<temp->value<<endl;
    temp = temp->next;
  }
}

void unitTest()
{
  dynamicArray<int> *testArray = new dynamicArray<int>;
  int inputArray[7] = {-1,3,5,8,1,0,1};
  int outputArray[7];
  int testValue = 17;
  int arrayLength;
  cout<<"This is the unit tester"<<endl; 
  cout<<"Testing preAllocateMemory and getAllocatedMemory functions ";
  testArray->preAllocateMemory(5);
  assert(testArray->getAmtPositionsAllocatedToMemory() == 5);
  cout<<"[PASS]"<<endl;

  cout<<"Testing insertAtIndex and retrieveFromIndex functions ";
  for(int i =-1; i<9; i++)
    {
      testArray->insertAtIndex(i,inputArray[i]);
      outputArray[i] = testArray->retrieveFromIndex(i);
    }
  for(int i = 0; i<7; i++)
    {
      assert(inputArray[i] == outputArray[i]);
    }
  cout<<"[PASS]"<<endl;

  cout<<"Testing insertAtHead, InsertAtTail, getLengthArray, and removeFromIndex functions ";
  testArray->insertAtHead(testValue);
  assert(testArray->retrieveFromIndex(0)==testValue);
  testArray->insertAtTail(testValue);
  arrayLength = testArray->getLengthArray();
  assert(testArray->retrieveFromIndex(arrayLength-1) == testValue);
  testArray->removeFromIndex(0);
  assert(testArray->getLengthArray()<arrayLength);
  cout<<"[PASS]"<<endl;
  
}

int main()
{
  dynamicArray<int> *testArray = new dynamicArray<int>;
  int choice = 0;
  int x = 0;
  int index = 0;
  
  while(choice != 11)
    {
      cout<<"Welcome to Dillon Connolly's Dynamic Array" <<endl;
      cout<<"Input 0 for unit test or 1-11 for manual testing below"<<endl;
      cout<<"1: insertAtHead function" <<endl;
      cout<<"2: insertAtTail function" <<endl;
      cout<<"3: insertAtIndex function" <<endl;
      cout<<"4: displayContents function" <<endl;
      cout<<"5: swapItemsIndex function" <<endl;
      cout<<"6: retrieveFromIndex function" <<endl;
      cout<<"7: removeFromIndex function" <<endl;
      cout<<"8: getLengthArray function" <<endl;
      cout<<"9: getAmtPositionsAllocatedToMemory function" <<endl;
      cout<<"10: preAllocateMemory function" <<endl;
      cout<<"11: Exit program" <<endl;
  cin>>choice; 
  cout<<endl;
  switch(choice)
    {
    case 0:
      unitTest();
      break;
    case 1:
      cout<<"Enter value to insert at the head of the array: ";
      cin>>x;
      testArray->insertAtHead(x);
      cout<<endl;
      break; 
    case 2:
      cout<<"Enter value to insert at the tail of the array: ";
      cin>>x;
      testArray->insertAtTail(x);
      cout<<endl;
      break;
     case 3:
      cout<<"Enter value to insert: ";
      cin>>x;
      cout<<endl;
      cout<<"Enter the index to insert the value at: ";
      cin>>index;
      cout<<endl;
      testArray->insertAtIndex(index,x);
      break;
    case 4:
      cout<<"Contents of the array:"<<endl;
      testArray->getContentsOfArray();
      break;
    case 5:
      cout<<"Enter index 1: ";
      cin>>x; //Index 1
      cout<<endl;
      cout<<"Enter index 2: ";
      cin>>index; //Index 2
      cout<<endl;
      testArray->swapItemsIndex(x,index);
      break;
    case 6:
      cout<<"Enter index to retrieve data from: ";
      cin>>index;
      cout<<endl;
      cout<<testArray->retrieveFromIndex(index);
      break;
     case 7:
      cout<<"Enter index to remove: ";
      cin>>index;
      cout<<endl;
      testArray->removeFromIndex(index);
      break;
    case 8:
      cout<<"Array length:"<<endl;
      cout<<testArray->getLengthArray()<<endl;
      break;
    case 9:
      cout<<"Amount of memory allocated: ";
      cout<<testArray->getAmtPositionsAllocatedToMemory()<<endl;;
      break;
    case 10:
      cout<<"Enter amount of memory to allocate: ";
      cin>>x;
      cout<<endl;
      testArray->preAllocateMemory(x);
      break;
    default:
      break;
    }

    }

  return 0;
}
